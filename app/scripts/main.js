/*global $:false, Vue:false */

$('document').ready(function() {
  'use strict';

  var conf = {
    key: 'y4ag8wf82um44kuftbmc9pmz',
    base: 'https://api.edmunds.com/api/vehicle/v2/',
    service: 'https://api.edmunds.com/v1/api/maintenance/'
  };



  /**
   * helper functions - not vue specific
   */
  var Helpers = (function() {

    return {

      logError: function(error) {
        var response = JSON.parse(error.responseText);
        console.log('RESPONSE ERROR [' + response.status + '] ' + response.errorType + ' - ' + response.message);
      },

      resetBreadcrumbs: function() {

        var menuLabels = ['Vehicle Make', 'Model', 'Year', 'Trim Level', 'Profile'];
        var limit = menuLabels.length;

        for(var i = 0; i < limit; i++) {
          $('.breadcrumb li:eq("' + i + '")').empty().html(menuLabels[i]);
        }
      }
    };
  })();



  /**
   * VUE COMPONENTS
   */

  Vue.component('models-comp', {
    props: ['models', 'obj']
  });

  Vue.component('years-comp', {
    props: ['obj', 'years']
  });

  Vue.component('trims-comp', {
    props: ['obj', 'trims']
  });

  Vue.component('profile-comp', {
    props: ['obj']
  });


  /**
   * main vue instance
   */
  new Vue({
    el: '#vehicle-app',
    data: {
      makes: [],
      models: [],
      trims: [],
      years: [],
      obj: {
        make: '',
        make_display: '',
        model: '',
        model_display: '',
        year: 1900,
        trim_id: '',
        trim_name: '',
        profile: {},
        safety: {}
      },
      menu: ['Vehicle Make', 'Model', 'Year', 'Trim Level', 'Profile'],
      show: {
        makes: false,
        models: false,
        years: false,
        trims: false,
        profile: false
      }
    },
    ready: function() {

      this.getVehicleMakes();
    },

    methods: {

      getVehicleMakes: function() {

        var self = this;

        $.get(conf.base + 'makes', { fmt: 'json', view: 'full', api_key: conf.key })
        .fail(function(error) {
          Helpers.logError(error);
        })
        .done(function(results) {
          self.makes = results.makes;

          //show makes section
          self.show.makes = true;
        });
      },

      getModelsByMake: function(make) {

        var self = this;

        $.get(conf.base + make, { fmt: 'json', view: 'full', api_key: conf.key })
        .fail(function(error) {
          Helpers.logError(error);
        })
        .done(function(results) {
          self.models = results.models;

          //transition sections
          self.show.makes = false;
          self.show.models = true;

        });
      },

      getYearsByMakeAndModel: function(make, model) {

        var self = this;

        $.get(conf.base + make + '/' + model + '/years', { fmt: 'json', view: 'full', api_key: conf.key })
        .fail(function(error) {
          Helpers.logError(error);
        })
        .done(function(results) {
          self.years = results.years;

          //transiton sections
          self.show.models = false;
          self.show.years = true;
        });
      },

      getTrimLevels: function(make, model, year) {

        var self = this;

        $.get(conf.base + make + '/' + model + '/' + year + '/styles', { fmt: 'json', view: 'full', api_key: conf.key })
        .fail(function(error) {
          Helpers.logError(error);
        })
        .done(function(results) {
          self.trims = results.styles;

          //transition sections
          self.show.years = false;
          self.show.trims = true;
        });
      },

      getVehicleProfile: function(id) {

        var self = this;

        $.get(conf.base + 'styles/' + id, { fmt: 'json', view: 'full', api_key: conf.key })
        .fail(function(error) {
          Helpers.logError(error);
        })
        .done(function(results) {
          self.obj.profile = results;

          //transition sections
          self.show.trims = false;
          self.show.profile = true;
        });


        //get safety rating information
        $.get(conf.base + 'styles/' + id + '/safety', { fmt: 'json', view: 'full', api_key: conf.key })
        .fail(function(error) {
          Helpers.logError(error);
        })
        .done(function(results) {
          self.obj.safety = results;
        });
      },

      setSelectedVehicle: function(event) {
        var nameSegs = event.target.id.split(':');

        this.$set('obj.make_display', nameSegs[0]);
        this.$set('obj.make', nameSegs[1]);

        this.getModelsByMake(this.obj.make);
      },

      setSelectedModel: function(event) {
        var modelSegs = event.target.id.split(':');

        this.$set('obj.model_display', modelSegs[0]);
        this.$set('obj.model', modelSegs[1]);

        this.getYearsByMakeAndModel(this.obj.make, this.obj.model);
      },

      setSelectedYear: function(event) {
        var year = event.target.id.split('-')[1];
        this.$set('obj.year', year);

        this.getTrimLevels(this.obj.make, this.obj.model, this.obj.year);
      },

      setSelectedTrim: function(event) {
        var trim = event.target.id.split('-')[1];
        var name = event.explicitOriginalTarget.innerHTML;

        this.$set('obj.trim_id', trim);
        this.$set('obj.trim_name', name);

        this.getVehicleProfile(trim);
      },

      reset: function() {

        console.log('init reset');
        this.$set('show.makes', true);
        this.$set('show.models', false);
        this.$set('show.years', false);
        this.$set('show.trims', false);
        this.$set('show.profile', false);

        //reset breadcrumbs
        Helpers.resetBreadcrumbs();
      }
    },

    computed: {
      profileTitle: function() {

        var output = '';

        if(this.obj.year > 1900) {
          output = this.obj.year + ' ' + this.obj.make_display + ' ' + this.obj.model_display;
        } else {
          output = this.obj.make_display + ' ' + this.obj.model_display;
        }

        return output;
      },

      profileOverview: function() {
        return this.obj.profile.year.year + ' ' + this.obj.profile.submodel.modelName + ' ' + this.obj.profile.trim;
      },

      profileEngine: function() {
        var output = '';
        output += this.obj.profile.engine.size + 'L ';
        output += this.obj.profile.engine.configuration + ' ' + this.obj.profile.engine.cylinder + ' cylinder';
        return output;
      },

      profileTransmission: function() {
        var output = '';
        output += this.obj.profile.transmission.transmissionType + ' ';
        output += this.obj.profile.transmission.equipmentType + ' ';
        output += this.obj.profile.transmission.numberOfSpeeds + ' speeds';
        return output;
      }
    }
  });


  /**
   * New Vue instance for landing/jumbotron
   * .container vs .container-fluid classes conflicted resulting in body margin, therefore
   * jumbotron is not nested inside of #vehicle-app
   */
  new Vue({
    el: '#landing',
    data: {
      show: true
    },
    methods: {
      start: function() {

        this.show = false;
      }
    }
  });

});
